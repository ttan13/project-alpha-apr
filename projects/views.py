from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin

from projects.models import Project


class ProjectListView(
    LoginRequiredMixin, ListView
):  # Protect list view so only person logged in can access
    model = Project
    template_name = "projects/list.html"
    context_object_name = "projects"

    def get_queryset(self):  # Change the queryset of the view
        return Project.objects.filter(  # filter the Project objects
            members=self.request.user  # members equals the logged in user
        )


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]
    # success_url = reverse_lazy("show_project")

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])
