from django.urls import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from tasks.models import Task

# Create your views here.


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_success_url(
        self,
    ):  # when the view successfully handles the form submission
        return reverse_lazy(
            "show_project", args=[self.object.id]
        )  # redirect to the detail page of the task's project


class TaskListView(
    LoginRequiredMixin, ListView
):  # view is only accessible by pple logged in
    model = Task
    template_name = "tasks/list.html"
    context_object_name = (
        "tasks"  # name in order to call w/i for and if loops w/i template tags
    )

    def get_queryset(self):
        # person only sees the tasks assigned to them by filtering w the
        # assignee = to the currently logged in user
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
