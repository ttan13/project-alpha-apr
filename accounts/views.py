from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login

# Create your views here.


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(  # create new user acct
                username=username,  # from username &
                password=password,  # password
            )
            user.save()  # save the user object
            login(
                request, user
            )  # use the login function that logs an account in
            return redirect(
                "home"
            )  # redirect the browser to the path registered
    else:
        form = UserCreationForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)
